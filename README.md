# Pre VS post increment in java loops

Demonstrates that in java, using `i++` or `++i` in a for loop generates the same bytecode and therefore has no impact on performances.

The goal was to determine whether in java `for (int i=0; i<n, ++i)` is slower or faster than `for (int i=0; i<n, i++)`, even though both are logically equivalent. 
This discussion arised because one of the students had heard that `++i` is faster than `i++`. 
A quick search on stackoverflow showed that both are equivalent in java and that this common misconception originates from C++ (is it faster or slower than ++C? :-) ).

I first setup to investigate by comparing the compiled versions of the pre and the post-increment, then a colleague showed me `javap -c` to look at the bytecode.
I kept both approaches.


# Test files

## TestIPlusPlus

The file `TestIPlusPlus.java` contains:

```java
public class TestIPlusPlus {

  public static void main(String[] args) {
    for (int i=0; i<5; i++) {
      System.out.println(i);
    }
  }

}
``` 


## TestPlusPlusI.java

The file `TestPlusPlusI.java` just replaced `i++` by `++i`:

```java
public class TestPlusPlusI {

  public static void main(String[] args) {
    for (int i=0; i<5; ++i) {
      System.out.println(i);
    }
  }

}
```


# The straightforward way with javap

The convenient [javap](https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javap.html) command disassembles one or more class files.


## i++ analysis

```bash
> javac TestIPlusPlus.java
> javap -c TestIPlusPlus
Compiled from "TestIPlusPlus.java"
public class TestIPlusPlus {
  public TestIPlusPlus();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_0
       1: istore_1
       2: iload_1
       3: iconst_5
       4: if_icmpge     20
       7: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      10: iload_1
      11: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
      14: iinc          1, 1
      17: goto          2
      20: return
}
```

Note line 14, which corresponds to the increment.


## ++i analysis

```bash
> javac TestPlusPlusI.java
> javap -c TestPlusPlusI
Compiled from "TestPlusPlusI.java"
public class TestPlusPlusI {
  public TestPlusPlusI();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_0
       1: istore_1
       2: iload_1
       3: iconst_5
       4: if_icmpge     20
       7: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      10: iload_1
      11: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
      14: iinc          1, 1
      17: goto          2
      20: return
}

```

Note line 14, which corresponds to the increment, is the same as in `TestIPlusPlus`.

## Comparison

The only difference between the pre and post-increment is related to the filenames.
Therefore, **using `i++` or `++i` in a loop results in the same bytecode**, which entails that it does not have an impact on the performances.

```bash
> javap -c TestIPlusPlus > TestIPlusPlus.txt
> javap -c TestPlusPlusI > TestPlusPlusI.txt
> diff TestIPlusPlus.txt TestPlusPlusI.txt
1,3c1,3
< Compiled from "TestIPlusPlus.java"
< public class TestIPlusPlus {
<   public TestIPlusPlus();
---
> Compiled from "TestPlusPlusI.java"
> public class TestPlusPlusI {
>   public TestPlusPlusI();

```




# The convoluted way

Here we compare directly the compiled `TestIPlusPlus.class` and `TestPlusPlusI.class` files.


## The compiled .class files have the same size

Both `.class` files are 448 bytes.

```bash
> javac TestIPlusPlus.java
> javac TestPlusPlusI.java
> ll Test*.class
-rw-r--r-- 1 olivier olivier 448 Nov 11 11:45 TestIPlusPlus.class
-rw-r--r-- 1 olivier olivier 448 Nov 11 11:46 TestPlusPlusI.class
```

## The compiled .class files are slightly different

A direct comparison of the `.class` files shows that there is a slight difference.

```bash
> cmp TestIPlusPlus.class TestPlusPlusI.class
TestIPlusPlus.class TestPlusPlusI.class differ: byte 140, line 4
```

## This difference is only related to the class names

We first generate an user-friendly dump of each `.class` file, and compare them.



```bash
> xxd TestIPlusPlus.class TestIPlusPlus.hex
> xxd TestPlusPlusI.class TestPlusPlusI.hex
```

Ok, so much for "user-friendly", here is what it actually looks like:

```bash
> cat TestIPlusPlus.hex 
00000000: cafe babe 0000 0037 001c 0a00 0500 0f09  .......7........
00000010: 0010 0011 0a00 1200 1307 0014 0700 1501  ................
00000020: 0006 3c69 6e69 743e 0100 0328 2956 0100  ..<init>...()V..
00000030: 0443 6f64 6501 000f 4c69 6e65 4e75 6d62  .Code...LineNumb
00000040: 6572 5461 626c 6501 0004 6d61 696e 0100  erTable...main..
00000050: 1628 5b4c 6a61 7661 2f6c 616e 672f 5374  .([Ljava/lang/St
00000060: 7269 6e67 3b29 5601 000d 5374 6163 6b4d  ring;)V...StackM
00000070: 6170 5461 626c 6501 000a 536f 7572 6365  apTable...Source
00000080: 4669 6c65 0100 1254 6573 7449 506c 7573  File...TestIPlus
00000090: 506c 7573 2e6a 6176 610c 0006 0007 0700  Plus.java.......
000000a0: 160c 0017 0018 0700 190c 001a 001b 0100  ................
000000b0: 0d54 6573 7449 506c 7573 506c 7573 0100  .TestIPlusPlus..
000000c0: 106a 6176 612f 6c61 6e67 2f4f 626a 6563  .java/lang/Objec
000000d0: 7401 0010 6a61 7661 2f6c 616e 672f 5379  t...java/lang/Sy
000000e0: 7374 656d 0100 036f 7574 0100 154c 6a61  stem...out...Lja
000000f0: 7661 2f69 6f2f 5072 696e 7453 7472 6561  va/io/PrintStrea
00000100: 6d3b 0100 136a 6176 612f 696f 2f50 7269  m;...java/io/Pri
00000110: 6e74 5374 7265 616d 0100 0770 7269 6e74  ntStream...print
00000120: 6c6e 0100 0428 4929 5600 2100 0400 0500  ln...(I)V.!.....
00000130: 0000 0000 0200 0100 0600 0700 0100 0800  ................
00000140: 0000 1d00 0100 0100 0000 052a b700 01b1  ...........*....
00000150: 0000 0001 0009 0000 0006 0001 0000 0001  ................
00000160: 0009 000a 000b 0001 0008 0000 0048 0002  .............H..
00000170: 0002 0000 0015 033c 1b08 a200 10b2 0002  .......<........
00000180: 1bb6 0003 8401 01a7 fff1 b100 0000 0200  ................
00000190: 0900 0000 1200 0400 0000 0400 0700 0500  ................
000001a0: 0e00 0400 1400 0700 0c00 0000 0900 02fc  ................
000001b0: 0002 01fa 0011 0001 000d 0000 0002 000e  ................
```

Anyway, now we can use `diff` to spot the differences:

```bash
> diff TestIPlusPlus.hex TestPlusPlusI.hex
9,10c9,10
< 00000080: 4669 6c65 0100 1254 6573 7449 506c 7573  File...TestIPlus
< 00000090: 506c 7573 2e6a 6176 610c 0006 0007 0700  Plus.java.......
---
> 00000080: 4669 6c65 0100 1254 6573 7450 6c75 7350  File...TestPlusP
> 00000090: 6c75 7349 2e6a 6176 610c 0006 0007 0700  lusI.java.......
12c12
< 000000b0: 0d54 6573 7449 506c 7573 506c 7573 0100  .TestIPlusPlus..
---
> 000000b0: 0d54 6573 7450 6c75 7350 6c75 7349 0100  .TestPlusPlusI..

```

The only differences are lines 9-10 and 12.
As seen previously, they are only related to the name of the file and of the class.
**Both `i++` and `++i` result in exactly the same code.**



